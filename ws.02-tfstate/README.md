# WS.02 - TFSTATE

* Descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git
* Copiar el fichero tfstate.sh  que se encuentra dentro de la carpeta ws.02-tfstate en la consola de cloud Shell de Azure y modificar los permisos de ejecución si es necesario.
* Editar el fichero y comprobar que acciones va a realizar
* Ejecutar el fichero con vuestras iniciales como parámetro de entrada y almacenar la salida del mismo para configurar el siguiente workshop

```
azureuser@Azure:~/clouddrive$ ./tfstate.sh jma
…..
…..
storage_account_name: tfstatejma
container_name: landing-zone
access_key: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

```

