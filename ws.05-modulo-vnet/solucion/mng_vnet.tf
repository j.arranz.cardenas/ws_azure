module "network-mng" {
  source                        = "./modules/network"
  name                          = format("%s-%s", var.name, "mng")
  location                      = var.location
  address_space                 = "10.1.0.0/16"

  subnets  = {
      dmz       = {
            address_prefixes  = ["10.1.0.64/27"]
            special_subnet  = false
      },
      workload  = {
            address_prefixes  = ["10.1.1.0/24"]
            special_subnet  = false
      },
      core      = {
            address_prefixes  = ["10.1.2.0/24"]
            special_subnet  = false
      }
      
  }
  common_tags = local.common_tags
}
