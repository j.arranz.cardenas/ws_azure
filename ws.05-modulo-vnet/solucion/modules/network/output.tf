output "rg_name" {
  description = "The Name of the resource group created"
  value       = azurerm_virtual_network.vnet.resource_group_name
}
output "vnet_id" {
  description = "ID de la vNet creada"
  value       = azurerm_virtual_network.vnet.id
}

output "vnet_name" {
  description = "The Name of the newly created vNet"
  value       = azurerm_virtual_network.vnet.name
}


output "vnet_address_prefix" {
  description = "The address space of the newly created vNet"
  value = [
    for subnet in azurerm_subnet.subnet:
      subnet.address_prefix
  ]
} 

 output "vnet_subnets_id" {
  description = "The ids of subnets created inside the new vNet"
  value = [
    for subnet in azurerm_subnet.subnet:
      subnet.id
  ]
} 
