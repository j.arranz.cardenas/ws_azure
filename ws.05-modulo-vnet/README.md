# WS.05 - Creación de un módulo para el despliegue de vnets
En caso de no haberlo hecho aún, descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git

1. Dentro de la carpeta `tf-landing-zone/modules` generar una carpeta `network` con los siguientes ficheros.
    * `main.tf` 
    * `variables.tf`
    * `outputs.tf`

2. Usando como guía el fichero mng_net.tf del ejercio anterior, generar un modulo que realice lo siguiente
    * Creación de un grupo de recursos que contenga
        * El nombre con formato `variable`-rg --> utilizar la función `format()`
        * Usar una variable para la localización del recurso
        * Usar una variable para asociar las tags al recurso y añadir un nuevo tag (Name) con el nombre del recurso. Para ello utilizar las funciones totomap() y merge ()
        * Outputs a definir:
            *   "rg_name" --> resource_group_name

    * Creación de una grupo VNET que contenga
        * El nombre con formato `variable`-vnet --> utilizar la función `format()`
        * Usar una variable para la localización del recurso
        * Usar una variable para definir el direccionamiento de la VNET
        * Usar una variable para asociar las tags al recurso y añadir un nuevo tag (Name) con el nombre del recurso. Para ello utilizar las funciones tomap() y merge ()
        * Outputs a definir:
            * "vnet_id" --> id
            * "vnet_name" --> name

3. Modificar el fichero mng_net.tf para que haga referencia al modulo generado y desplegar la infraestructura
    * Comprobar que estamos en el workspace pro
    * `terraform init`
    * `terraform plan -out plan.out`
    * `terraform apply plan.out`
    * Comprobar en el portal que se ha desplegado correctamente

4. Añadir al módulo la creación de las subredes 
    * Utilizando un solo recurso con un metadato `for_each` y almacenando la información en un mapa
    * El nombre de la subnet se creará formateando cada una de las keys del mapa con la variable que se ha definido previamente para nombrar el resto de recursos `variable`-`each.key`
    * El direccionamiento de cada subred sera el value del mapa
    * Outputs a definir:
        * [vnet_address_prefix] --> address_prefix (list)
        * [vnet_subnets_id] --> subnet (list)


5.  Modificar el fichero mng-net.tf para modificar las variables del módulo y desplegar la infraestructura
    * `terraform init`
    * `terraform plan -out plan.out`
    * `terraform apply plan.out`
    * Comprobar en el portal que se ha desplegado correctamente
  


