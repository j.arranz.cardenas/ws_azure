# WS.07 - Creación de la vnet hub para interconectar entornos
En caso de no haberlo hecho aún, descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git

En este último workshop vamos a generar la infraestructura necesaria para crear una vnet hub que centralice las comunicaciones con las otras 2 vnets previamente generadas y que se conectaran por medio de peerings
1. Dentro de la carpeta `tf-landing-zone/modules` generar una carpeta `connections` con los siguientes ficheros.
    * `main.tf` 
    * `variables.tf`
    * `outputs.tf`

2. La creación y asociación de "peerings" se realiza utilizando el recurso `azurerm_virtual_network_peering` y según la documentación de azure con terraform es necesario configuraro en ambos sentidos como se observa en el ejemplo

```
        resource "azurerm_virtual_network_peering" "spoke1-hub-peer" {
        name                      = "spoke1-hub-peer"
        resource_group_name       = azurerm_resource_group.spoke1-vnet-rg.name
        virtual_network_name      = azurerm_virtual_network.spoke1-vnet.name
        remote_virtual_network_id = azurerm_virtual_network.hub-vnet.id

        allow_virtual_network_access = true
        allow_forwarded_traffic = true
        allow_gateway_transit   = false
        use_remote_gateways     = true
        }

        resource "azurerm_virtual_network_peering" "hub-spoke1-peer" {
        name                      = "hub-spoke1-peer"
        resource_group_name       = azurerm_resource_group.hub-vnet-rg.name
        virtual_network_name      = azurerm_virtual_network.hub-vnet.name
        remote_virtual_network_id = azurerm_virtual_network.spoke1-vnet.id
        allow_virtual_network_access = true
        allow_forwarded_traffic   = true
        allow_gateway_transit     = true
        use_remote_gateways       = false
        }
```

Con esta información realizar un modulo que en un bucle configure los n peering necesarios para nuestro entorno (4)

4. Crear el fichero hub_vnet.tf  y desplegar la infraestructura con la siguiente configuración 
    * direccionamiento de la vnet  `10.10.0.0/16`
    * generar una única vnet dmz con direccion `10.10.0.64/27`
    * Comprobar en el portal que se ha desplegado correctamente

Después de esta ejecución tendriamos un entorno listo para comunicarse entre las vnet a falta de generar las tablas de rutas. Os animo a generar un nuevo módulo que automatitize la creación de dichas tablas de rutas.

Espero que el workshop os haya servido para empezar a trabajar con Terraform. 

Muchas gracias por vuestra atención


