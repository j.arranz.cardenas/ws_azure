module "network-hub" {
  source                        = "./modules/network"
  name                          = format("%s-%s", var.name, "hub")
  location                      = var.location
  address_space                 = "10.10.0.0/16"

  subnets  = {
      dmz       = {
            address_prefixes  = ["10.10.0.64/27"]
            special_subnet  = false
      }
      
  }
  common_tags = local.common_tags
}

module "hub-net-sg" {
  source                          = "./modules/net-sg"
  name                            = var.name
  location                        = var.location
  resource_group_name             = module.network-hub.rg_name
  network_security_rule_csv       = "./files/network_security_rule.csv"
  subnet_assoc_id                 = [module.network-hub.vnet_subnets_id[0]]
  
  common_tags                     = local.common_tags
}

module "connections" {
  source                          = "./modules/connections"
  resource_group_name             = module.network-mng.rg_name
  peering_connection              = {
    mng-hub-peer  = {
        resource_group_name       = module.network-hub.rg_name
        virtual_network_name      = module.network-hub.vnet_name
        remote_virtual_network_id = module.network-mng.vnet_id
    },
    hub-mng-peer  = {
        resource_group_name       = module.network-mng.rg_name
        virtual_network_name      = module.network-mng.vnet_name
        remote_virtual_network_id = module.network-hub.vnet_id
    },
    dev-hub-peer  = {
        resource_group_name       = module.network-hub.rg_name
        virtual_network_name      = module.network-hub.vnet_name
        remote_virtual_network_id = module.network-dev.vnet_id
    },
    hub-dev-peer  = {
        resource_group_name       = module.network-dev.rg_name
        virtual_network_name      = module.network-dev.vnet_name
        remote_virtual_network_id = module.network-hub.vnet_id
    }
  }
}