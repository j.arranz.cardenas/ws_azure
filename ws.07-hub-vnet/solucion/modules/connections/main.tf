# Creación de las conexiones entre las vnets
resource "random_password" "shared-key" {
  length                          = 10
  special                         = true
  override_special                = "_%@"
}

resource "azurerm_virtual_network_gateway_connection" "gateway-conn" {
  for_each                        = var.gateway_connection 
  location                        = var.location
  resource_group_name             = var.resource_group_name
  type                            = var.conn_type
  routing_weight                  = var.routing_weight
  shared_key                      = random_password.shared-key.result

  name                            = each.key
  virtual_network_gateway_id      = each.value.virtual_network_gateway_id
  peer_virtual_network_gateway_id = each.value.peer_virtual_network_gateway_id
}

resource "azurerm_virtual_network_peering" "peering-conn" {
  for_each                        = var.peering_connection
  
  allow_virtual_network_access    = var.allow_virtual_network_access
  allow_forwarded_traffic         = var.allow_forwarded_traffic
  allow_gateway_transit           = var.allow_gateway_transit
  use_remote_gateways             = var.use_remote_gateways

  name                            = each.key
  resource_group_name             = each.value.resource_group_name
  virtual_network_name            = each.value.virtual_network_name
  remote_virtual_network_id       = each.value.remote_virtual_network_id
}