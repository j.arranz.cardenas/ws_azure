output "conn-shared-key" {
  description = "clave de encriptación de la conexión"
  value       = random_password.shared-key.result
}
 