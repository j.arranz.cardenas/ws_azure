variable "location" {
  description   = "Localización de la infra"
  type          = string
  default       = "eastus"
}

variable "conn_type" {
  description  = "Tipo de conexión entre vnets"
  type         = string 
  default      = "Vnet2Vnet"
}

variable "routing_weight" {
  description  = "Peso del enrutado"
  type         = number 
  default      = 1
}

variable "gateway_connection" {
  description = "mapa con los valores necesarios para generar las conexiones tipo gateway."
  type = map(object({
        virtual_network_gateway_id          = string
        peer_virtual_network_gateway_id     = string
      }))
  default     = {}
}

variable "peering_connection" {
  description = "mapa con los valores necesarios para generar las conexiones tipo peering."
  type = map(object({
        resource_group_name                 = string
        virtual_network_name                = string
        remote_virtual_network_id           = string
      }))
  default     = {}
}

variable "allow_virtual_network_access" {
  description  = "Permitir el acceso de la a las redes"
  type         = bool 
  default      = true
}

variable "allow_forwarded_traffic" {
  description  = "Permitir el trafico forwarded"
  type         = bool 
  default      = true
}

variable "allow_gateway_transit" {
  description  = "Permitir el transit gateway"
  type         = bool 
  default      = true
}

variable "use_remote_gateways" {
  description  = "Permitir el transit gateway"
  type         = bool 
  default      = false
}

variable "resource_group_name" {
  description  = "Nombre del recurso sobre el que se vá a desplegar"
  type         = string 
  default      = null
}