variable "location" {
  description   = "Localización de la infra"
  type          = string
  default       = "eastus"
}

variable "name" {
  description  = "Nombre generico para todo el despliegue"
  type         = string 
  default      = "default"
}

variable "resource_group_name" {
  description  = "Nombre del recurso sobre el que se vá a desplegar"
  type         = string 
  default      = null
}

variable "subnet_assoc_id" {
  description  = "id de las subredes a asociar al security group"
  type         = list
}
variable "network_security_rule_csv" {
  description = "csv con las caracteristicas de las reglas de acceso"
  type        = any
  default     = null
}

variable "common_tags" {
  description   = "Tags genericos para todo el despliegue"
  type          = map
  default       = {}
}