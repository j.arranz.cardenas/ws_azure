resource "azurerm_resource_group" "mgmt-vnet-rg" {
  name     = format("%s-%s", local.mgmt-prefix, "rg")
  location = local.mgmt-location
  tags     = local.common_tags
}

resource "azurerm_virtual_network" "mgmt-vnet" {
  name                = format("%s-%s", local.mgmt-prefix, "vnet")
  location            = azurerm_resource_group.mgmt-vnet-rg.location
  resource_group_name = azurerm_resource_group.mgmt-vnet-rg.name
  address_space       = ["10.1.0.0/16"]

  tags = local.common_tags
}

resource "azurerm_subnet" "mgmt-dmz" {
  name                 = format("%s-%s", local.mgmt-prefix, "dmz")
  resource_group_name  = azurerm_resource_group.mgmt-vnet-rg.name
  virtual_network_name = azurerm_virtual_network.mgmt-vnet.name
  address_prefixes     = ["10.1.0.64/27"]
}

resource "azurerm_subnet" "mgmt-workload" {
  name                 = format("%s-%s", local.mgmt-prefix, "workload")
  resource_group_name  = azurerm_resource_group.mgmt-vnet-rg.name
  virtual_network_name = azurerm_virtual_network.mgmt-vnet.name
  address_prefixes     = ["10.1.1.0/24"]
  
}

resource "azurerm_subnet" "mgmt-core" {
  name                 = format("%s-%s", local.mgmt-prefix, "core")
  resource_group_name  = azurerm_resource_group.mgmt-vnet-rg.name
  virtual_network_name = azurerm_virtual_network.mgmt-vnet.name
  address_prefixes     = ["10.1.2.0/24"]
}