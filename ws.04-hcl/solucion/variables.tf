variable "location" {
  description   = "Localización de la infra"
  type          = string
  default       = "eastus"
}

variable "name" {
  description  = "Nombre generico para todo el despliegue"
  type         = string 
  default      = "xxx" # Donde xxx son vuestras iniciales
}

variable "environment" {
 type = map
    default = {
    dev = "desarrollo"
    pre = "preproduccion"
    pro = "produccion"
    }
}