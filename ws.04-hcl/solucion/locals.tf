locals {
  mgmt-location = var.location
  mgmt-prefix   = "${var.name}-mgmt"
  common_tags = {
    Project             = "ws-almirall"
    CostCenter          = "ws-sl"
    Owner               = var.name
    environment         = lookup(var.environment, terraform.workspace)
  }
}