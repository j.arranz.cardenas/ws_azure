provider "azurerm" {
  features {}
}

terraform {
  required_version = "> 0.12.0"

  required_providers {
    azurerm = "~>2.0"
    null = "~> 2.0"
  }
    backend "azurerm" {
    resource_group_name   = "tfstate"
    storage_account_name  = "tfstatexxx" # Donde xxx son vuestras credenciales
    container_name        = "landing-zone-test"
    key                   = "terraform.tfstate"
  }
}

variable "location" {
  description   = "Localización de la infra"
  type          = string
  default       = "eastus"
}

variable "name" {
  description  = "Nombre generico para todo el despliegue"
  type         = string 
  default      = "xxx" # Donde xxx son vuestras credenciales
}

locals {
  mgmt-location = var.location
  mgmt-prefix   = "${var.name}-mgmt"
}

resource "azurerm_resource_group" "mgmt-vnet-rg" {
  name     = "${local.mgmt-prefix}-rg"
  location = local.mgmt-location
  tags = {
    Project             = "ws-almirall"
    CostCenter          = "ws-sl"
    Owner               = "xxx"
    Environment         = "pro"
  }
}

resource "azurerm_virtual_network" "mgmt-vnet" {
  name                = "xxx-mgmt-vnet"
  location            = azurerm_resource_group.mgmt-vnet-rg.location
  resource_group_name = azurerm_resource_group.mgmt-vnet-rg.name
  address_space       = ["10.1.0.0/16"]

  tags = {
    Project             = "ws-almirall"
    CostCenter          = "ws-sl"
    Owner               = "xxx"
    Environment         = "pro"
  }
}

resource "azurerm_subnet" "mgmt-dmz" {
  name                 = "xxx-mgmt-dmz"
  resource_group_name  = azurerm_resource_group.mgmt-vnet-rg.name
  virtual_network_name = azurerm_virtual_network.mgmt-vnet.name
  address_prefixes       = ["10.1.0.64/27"]
}

resource "azurerm_subnet" "mgmt-workload" {
  name                 = "xxx-mgmt-workload"
  resource_group_name  = azurerm_resource_group.mgmt-vnet-rg.name
  virtual_network_name = azurerm_virtual_network.mgmt-vnet.name
  address_prefixes       = ["10.1.1.0/24"]
  
}
