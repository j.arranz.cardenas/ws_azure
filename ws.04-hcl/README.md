# WS.04 - Organización de ficheros terrafor
En caso de no haberlo hecho aún, descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git

1. Dentro de la carpeta `tf-landing-zone` restructurar el fichero main.tf, según las mejores practicas de terraform.
    * `mng_net.tf` (nombre de la vnet que vamos a generar)
    * `provider.tf`
    * `variables.tf`
    * `locals.tf`
2. Crear un worspace para desplegar la infraestructura (producción)
    * Comprobar en que workspace estamos `terraform workspace show`
    * Crear un workspace pro  `terraform workspace new pro`
    * Comprobar de nuevo en que workspace estamos ahora `terraform workspace show`
3. Modificar el código para evitar la repetición de partes de código y limitar el harcoding
    * Convertir los tags en locals
    * Eliminar el valor hardcodeado owner de los tags
    * cambiar los nombres de los recursos concatenando el local mgmt-prefix con los sufijos propios de cada recurso (utilizar la función `format()`)
    * Eliminar el valor harcodeado `Environment` por (Produccion, integacion o desarrollo) en función del valor que pueda tener el workspace definido (pro, int,dev) (esta informacións se puede almacenar en un locals o en una variable)
4. Desplegar la infraestructura
    * `terraform plan -out plan.out`
    * `terraform apply plan.out`
    * Comprobar en el portal que se ha desplegado correctamnete
5. Generar una nueva subnet llamada "xxx-mgmt-workload" y direccionamiento `"10.1.1.0/24"`

6. Desplegar de nuevo la infraestructura
    * `terraform plan -out plan.out`
    * `terraform apply plan.out`
    * Comprobar en el portal que se ha desplegado correctamnete
7. Destruir la infraestructura


