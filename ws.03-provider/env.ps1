echo "Setting environment variables for Terraform"
$env:ARM_SUBSCRIPTION_ID="your_subscription_id"
$env:ARM_CLIENT_ID="your_appId"
$env:ARM_CLIENT_SECRET="your_password"
$env:ARM_TENANT_ID="your_tenant_id"

# Not needed for public, required for usgovernment, german, china
$env:ARM_ENVIRONMENT="public"

# storage tfstate
$env:ARM_ACCESS_KEY="your_storage_tfstate_key"
