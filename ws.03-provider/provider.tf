provider "azurerm" {
  features {}
}

terraform {
  required_version = "> 0.12.0"

  required_providers {
    azurerm = "~>2.0"
    null = "~> 2.0"
  }
    backend "azurerm" {
    resource_group_name   = "tfstate-rg"
    storage_account_name  = "tfstatexxx" # Donde xxx son vuestras credenciales
    container_name        = "landing-zone"
    key                   = "terraform.tfstate"
  }
}
