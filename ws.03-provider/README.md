# WS.03 - PROVIDER

En caso de no haberlo hecho aún, descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git

## 1. **Creación del registro de aplicaciones**

Abrir en la consola de azure la "cloud shell" y ejecutar los siguientes comandos

* Comprobar la subcripción de azure en la que estamos conectados
```
az account list --query "[].{name:name, subscriptionId:id, tenantId:tenantId}"
[
  {
    "name": "Microsoft Azure Enterprise",
    "subscriptionId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx",
    "tenantId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx"
  }
]
```
* En caso de tener varias subscripciones, seleccionar la que se vaya a utilizar, exportandola a una variable de entorno. Una vez echo esto, ubicarse en la cuenta correcta.
```
export SUBSCRIPTION_ID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx
az account set --subscription="${SUBSCRIPTION_ID}"
```

* Crear el registro de aplicaciones para poder conectar terraform con el proveedor de Azure
```
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/${SUBSCRIPTION_ID}" --name "terraform-xxx" #Donde xxx son vuestras iniciales
{
  "appId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx",
  "displayName": "terraform-xxx",
  "name": "http://terraform-xxx",
  "password": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx",
  "tenant": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx"
}
```
* Almacenar la salida de los comandos y cerrar la CloudShell  

## 2. **Definición de las variables del sistema**

* Dentro del entrono que se va a usar para deplegar terraform (Cloud Shell, wsl shell, git bash), generar un script para cargar las credenciales como variables del sitema y darle permisos de ejecución. También se añadirá la credencial para poder almacenar el tfstate el el almacenamiento de azure.

### Linux

**env.sh**
```
#!/bin/sh
echo "Setting environment variables for Terraform"
export ARM_SUBSCRIPTION_ID=your_subscription_id
export ARM_CLIENT_ID=your_appId
export ARM_CLIENT_SECRET=your_password
export ARM_TENANT_ID=your_tenant_id

# Not needed for public, required for usgovernment, german, china
export ARM_ENVIRONMENT=public

# storage tfstate
export ARM_ACCESS_KEY=your_storage_tfstate_key
```
Ejecutar el script (`source env.sh`). Recordar que es necesario ejecutarlo cada vez que se abra una nueva terminal o configurar en el fichero `.bashrc` que se ejecute al abrir la sesión

### Windows

**env.ps1**
```
echo "Setting environment variables for Terraform"
$env:ARM_SUBSCRIPTION_ID="your_subscription_id"
$env:ARM_CLIENT_ID="your_appId"
$env:ARM_CLIENT_SECRET="your_password"
$env:ARM_TENANT_ID="your_tenant_id"

# Not needed for public, required for usgovernment, german, china
$env:ARM_ENVIRONMENT="public"

# storage tfstate
$env:ARM_ACCESS_KEY="your_storage_tfstate_key"
```
Ejecutar el script (`.\env.ps1`). Recordar que es necesario ejecutarlo cada vez que se abra una nueva terminal o configurar un profile que se ejecute al abrir la sesión


## 3. Creación de la estructura de directorios para el código terraform.

Generar la siguiente estructura de directorios para su uso durante las prácticas
```
tf-landing-zone
        |____files
        |____modules
```
4. Generar dentro de la carpeta tf-landing-zone un fichero provider.tf donde se debe configurar el proveedor de azurerm y la configuración para almacenar el tfstate en nuestro propio storage account.

5. Ejecutar el comando `terraform init` para inicializar el proyecto

```
$ terraform init

Initializing the backend...

Successfully configured the backend "azurerm"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "null" (hashicorp/null) 2.1.2...
- Downloading plugin for provider "azurerm" (hashicorp/azurerm) 2.6.0...

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```