# WS.06 - Creación de un modulo de network security group
En caso de no haberlo hecho aún, descargar o clonar el repositorio con los workshops del curso https://gitlab.com/j.arranz.cardenas/ws_azure.git

1. Dentro de la carpeta `tf-landing-zone/modules` generar una carpeta `net-sg` con los siguientes ficheros.
    * `main.tf` 
    * `variables.tf`
    * `outputs.tf`

2. Copiar el fichero csv adjunto con las reglas firewall en la carpeta files

3. La creación y asociación de "network security groups" implica la utilización de 3 recursos:
    * Creación de un recurso "azurerm_network_security_group" que contenga
        * El nombre con formato `variable`-rg --> utilizar la función `format()`
        * Una variable para la localización del recurso
        * Una variable para definir el nombre del grupo de recursos al que pertenece
        * Usar una variable para asociar las tags al recurso y añadir un nuevo tag (Name) con el nombre del recurso. Para ello utilizar las funciones tomap() y merge () 

    * Creación de un recurso "azurerm_network_security_rule" que lea de un fichero csv los valores de las reglas de acceso, para ello es necesario utilizar las siguientes funciones y metadatos 
        * Funcion `file()`
        * Función `csvdecode()` --> En la documentación de esta función hay un ejemplo de su utilización con un bucle `for_each`
        * Una variable para definir el nombre del grupo de recursos al que pertenece
        * Relacionarlo con el "azurerm_network_security_group" previamente generado

    * Creación de un recurso "azurerm_subnet_network_security_group_association" para asociar este security group con las n subredes que se definan. En este caso se va a utilizar `count` para leer de una lista las subredes a las que se va a asociar

    * Outputs a definir:
        * `net_sg_id` --> id (El identificador del security group generado.)

4. Modificar el fichero mng_net.tf para que haga referencia al modulo generado y desplegar la infraestructura
    * `terraform init`
    * `terraform plan -out plan.out`
    * `terraform apply plan.out`
    * Comprobar en el portal que se ha desplegado correctamente

5. Generar un fichero dev_vnet.tf que despliegue una nueva infraestructura con las mismas subredes y direccionamiento de la v-net `10.2.0.0/16`


