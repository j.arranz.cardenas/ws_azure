module "network-mng" {
  source                        = "./modules/network"
  name                          = format("%s-%s", var.name, "mgmt")
  location                      = var.location
  address_space                 = "10.1.0.0/16"

  subnets  = {
      dmz       = {
            address_prefixes  = ["10.1.0.64/27"]
            special_subnet  = false
      },
      workload  = {
            address_prefixes  = ["10.1.1.0/24"]
            special_subnet  = false
      },
      core      = {
            address_prefixes  = ["10.1.2.0/24"]
            special_subnet  = false
      }
      
  }
  common_tags = local.common_tags
}

module "mng-net-sg" {
  source                          = "./modules/net-sg"
  name                            = var.name
  location                        = var.location
  resource_group_name             = module.network-mng.rg_name
  network_security_rule_csv       = "./files/network_security_rule.csv"
  subnet_assoc_id                 = [module.network-mng.vnet_subnets_id[0], module.network-mng.vnet_subnets_id[1], module.network-mng.vnet_subnets_id[2]]
  
  common_tags                     = local.common_tags
}