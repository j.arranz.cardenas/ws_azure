module "network-dev" {
  source                        = "./modules/network"
  name                          = format("%s-%s", var.name, "dev")
  location                      = var.location
  address_space                 = "10.2.0.0/16"

  subnets  = {
      dmz       = {
            address_prefixes  = ["10.2.0.64/27"]
            special_subnet  = false
      },
      workload  = {
            address_prefixes  = ["10.2.1.0/24"]
            special_subnet  = false
      },
      core      = {
            address_prefixes  = ["10.2.2.0/24"]
            special_subnet  = false
      }
      
  }
  common_tags = local.common_tags
}

module "dev-net-sg" {
  source                          = "./modules/net-sg"
  name                            = var.name
  location                        = var.location
  resource_group_name             = module.network-dev.rg_name
  network_security_rule_csv       = "./files/network_security_rule.csv"
  subnet_assoc_id                 = [module.network-dev.vnet_subnets_id[0], module.network-dev.vnet_subnets_id[1], module.network-dev.vnet_subnets_id[2]]
  
  common_tags                     = local.common_tags
}