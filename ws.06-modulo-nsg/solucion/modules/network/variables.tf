variable "location" {
  description   = "Localización de la infra"
  type          = string
  default       = "eastus"
}

variable "name" {
  description  = "Nombre generico para todo el despliegue"
  type         = string 
  default      = "default"
}

variable "address_space" {
  description = "Espacio de direcciones asociado a las subnets"
  type        = string
  default     = "10.0.0.0/16"
}

# If no values specified, this defaults to Azure DNS 
variable "dns_servers" {
  description = "The DNS servers to be used with vNet."
  default     = []
}

variable "subnets" {
  description = "mapa con los valores necesarios para definir las subredes "
  type = map(object({
        address_prefixes     = list(string)
        special_subnet     = bool
      }))
}

variable "common_tags" {
  description   = "Tags genericos para todo el despliegue"
  type          = map
  default       = {}
}

variable "subnet_tags" {
  description   = "Tags específicos para las subredes"
  type          = map
  default       = {}
}
