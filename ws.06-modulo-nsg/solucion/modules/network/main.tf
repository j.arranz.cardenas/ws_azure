resource "azurerm_resource_group" "rg" {
  name                = format("%s-%s", var.name, "rg")
  location            = var.location
  tags                = merge(var.common_tags,tomap({"Name" = format("%s-%s", var.name, "rg")}))
}

resource "azurerm_virtual_network" "vnet" {
  name                = format("%s-%s", var.name, "vnet")
  location            = var.location
  address_space       = [var.address_space]
  resource_group_name = azurerm_resource_group.rg.name
  dns_servers         = var.dns_servers
  tags                = merge(var.common_tags,tomap({"Name" = format("%s-%s", var.name, "vnet")}))
}

resource "azurerm_subnet" "subnet" {
  for_each              = var.subnets
  name                  = each.value.special_subnet ? each.key : format("%s-%s", var.name, each.key)
  virtual_network_name  = azurerm_virtual_network.vnet.name
  resource_group_name   = azurerm_resource_group.rg.name
  address_prefixes      = each.value.address_prefixes

}