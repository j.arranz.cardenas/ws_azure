# Definición del "network security group"
resource "azurerm_network_security_group" "net-sg" {
  name                = format("%s-%s", var.name, "netsg")
  location            = var.location
  resource_group_name = var.resource_group_name
  tags                = merge(var.common_tags, tomap({"Name" = format("%s-%s", var.name, "netsg")}))
}

locals {
  network_security_rule = var.network_security_rule_csv != null ? csvdecode(file(var.network_security_rule_csv)) : []
}


# Definición de las reglas de acceso del "network security group"

resource "azurerm_network_security_rule" "net_sg_rule" {
  for_each                    = { for rule in local.network_security_rule : rule.name => rule }
  name                        = each.key
  priority                    = lookup(each.value, "priority", "1000")
  direction                   = lookup(each.value, "direction", "Inbound")
  access                      = lookup(each.value, "access", "Allow")
  protocol                    = lookup(each.value, "protocol", "*")
  source_port_range           = lookup(each.value, "source_port_range", "*")
  destination_port_range      = lookup(each.value, "destination_port_range", "*")
  source_address_prefix       = lookup(each.value, "source_address_prefix", "*")
  destination_address_prefix  = lookup(each.value, "destination_address_prefix", "*")
  resource_group_name         = var.resource_group_name
  network_security_group_name = azurerm_network_security_group.net-sg.name
}


 resource "azurerm_subnet_network_security_group_association" "mgmt-association" {
  count                       = var.subnet_assoc_id != null ? length(var.subnet_assoc_id): 0
  subnet_id                   = var.subnet_assoc_id[count.index]
  network_security_group_id   = azurerm_network_security_group.net-sg.id
}
