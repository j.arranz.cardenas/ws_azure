 output "net_sg_id" {
  description = "El identificador del security group generado."
  value = azurerm_network_security_group.net-sg.id
} 